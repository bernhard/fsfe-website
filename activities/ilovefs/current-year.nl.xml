<?xml version="1.0" encoding="UTF-8"?>
	
	<data>
		<version>4</version>
		<module>
			<h2>Ik ♥ Vrije Software dag 2024: Smeed de toekomst van Vrije Software</h2>
	
		<p>De "Ik hou van Vrije Software dag" is een traditie die al jaren duurt. Op deze dag bedanken we alle mensen achter Vrije Software-projecten en -programma's en laten we hen weten dat hun werk niet onopgemerkt blijft. Laten we samen alle geweldige mensen die werken voor Vrije Software vieren. Daarom bent u uitgenodigd om deel te nemen aan de "Ik hou van Vrije Software dag" in heel Europa. Sluit u aan bij uw <a href="/community/groups.en.html">lokale FSFE-groep</a> en organiseer een leuke bijeenkomst.</p>
	
			<p>We zijn dankbaar voor iedereen die werkt en bijdraagt aan het Vrije Software-universum: ontwikkelaars, ontwerpers, vertalers, bepleiters, donateurs, gebruikers en meer. Er zijn veel verschillende Vrije Softwaregemeenschappen, met verschillende waarden en verschillende focussen in hun werk. Maar we delen allemaal onze liefde voor Vrije Software en waarderen de vrijheid om software te gebruiken, te bestuderen, te delen en te verbeteren. Laten we onze waardering voor Vrije Software en de mensen er achter vieren met een simpel <q>bedankje!</q></p>
	
		<p>De editie van dit jaar heeft als thema “Smeed de toekomst van Vrije Software” omdat we de focus willen richten op de kritische waarde van nieuwe generaties. We willen jongeren betrekken bij de viering, zowel door jonge bijdragers te bedanken als wel door de principes van Vrije Software te introduceren bij degenen die hier zich nog niet van bewust waren. U kunt hier deel van uitmaken door mee te doen met de vieringen en een bijeenkomst in uw lokale gebied te organiseren.</p>
	
			<h2>Neem deel aan het feest</h2>
			
			<p>Wilt u met andere Vrije Software-enthousiastelingen vieren? We zullen snel bijeenkomsten door heel Europa heen bekendmaken! Volg ons op <a href="https://mastodon.social/@fsfe">sociale media</a> om niets te missen!</p>
			<p>Voel u vrij om ons via <a href="/contact/">e-mail</a> te benaderen als u een evenement in uw lokale gebied wil organiseren. Voor "Ik hou van Vrije Software dag 2024" willen we evenementen organiseren die gericht zijn op jonge mensen. Hier zijn enkele ideeën:</p>
			<ul class="ilovefs-ul">
			<li>Neem contact op met uw lokale bibliotheek en organiseer een lezing van ons kinderboek <a href="/activities/ada-zangemann/">"Ada &amp; Zangemann"</a></li>
			<li>Neem contact op met uw <a href="https://wiki.hackerspaces.org/List_of_Hacker_Spaces">lokale hackerspace</a> en organiseer een ontmoeting met hen</li>
			<li>Neem contact op met uw lokale jongerenclub en organiseer daar een evenement met lezingen over Vrije Software, praktische werkgroepen en pizza!</li>
			<li>Benader uw lokale Linux gebruikersgroep of uw <a href="/about/groups.html">lokale Vrije Softwaregroep</a> en organiseer samen een evenement!</li>
			</ul>
			
			
			<div class="icon-grid">
				<ul>
					<li>
						<img src="/graphics/icons/ilovefs-use.png" 
							alt="ILoveFS-hart met “use”"
						/>
						<div>
							<h3>Bedank<br/></h3>
							<p>             Bedank de mensen die u in staat stellen van uw softwarevrijheid te genieten. Organiseer een lokale 
              groepsbijeenkomst of verspreid de boodschap over Vrije Software. U kunt onze <a 
              href="https://download.fsfe.org/campaigns/ilovefs/share-pics/">sharepics</a>,
              <a href="/contribute/spreadtheword#ilovefs">stickers en
              ballonnen</a>, <a href="/activities/ilovefs/artwork/artwork.html">kunst</a> en
              <a href="/order/order.html">fan-artikelen</a> voor <em>#ilovefs</em> bestellen.</p>
						</div>
					</li>
					<li>
						<img src="/graphics/icons/ilovefs-study.png" 
							alt="ILoveFS-hart met 'study'"
						/>
						<div>
							<h3>Onthou de goede tijden<br/></h3>
						<p>Welk Vrije Softwareproject heeft u in het afgelopen jaar gebruikt? Waarmee voelde u zich sterker? Wat was leuk? Kijk voor enige inspiratie naar de <a href="/activities/ilovefs/whylovefs/gallery.html">liefdesverklaringen</a> en <a href="https://media.fsfe.org/w/p/u3Ep8hRP5vFdbgUE9LKScW">video's</a> van anderen.</p>
						</div>
					</li>
					<li>
						<img 
							src="/graphics/icons/ilovefs-share.png" 
							alt="ILoveFS-hart met 'deel'"
						/>
						<div>
							<h3>Deel uw liefde</h3>
						<p>Dat is het leuke deel! We hebben een <a href="https://sharepic.fsfe.org/#ilovefs">sharepic generator</a> voor u gemaakt waarmee u gemakkelijk uw eigen sharepics kunt maken en uw waardering kunt delen op sociale media (<em>#ilovefs</em>), in blogs, afbeeldingen en <a href="https://media.fsfe.org/w/p/u3Ep8hRP5vFdbgUE9LKScW">videoberichten</a> of <a href="/contribute/spreadtheword.html#ilovefs">direct aan Vrije Software-ontwikkelaars en -bijdragers</a>.</p>
						</div>
					</li>
					<li>
						<img 
							src="/graphics/icons/ilovefs-improve.png" 
							alt="ILoveFS-hart met 'verbeter'"
						/>
						<div>
							<h3>Verbeter Vrije Software</h3>
							<p>Spreken is zilver, bijdragen is goud! Help een Vrije Softwareproject met code, een vertaling of door gebruikers bij te staan. Overweeg, als u kan, alstublieft om ons werk te steunen via een <a href="/donate">donatie aan de Free Software Foundation Europe</a>.</p>
						</div>
					</li>
				</ul>
			</div>
	
			<figure class="no-border">
				<img
					src="https://pics.fsfe.org/uploads/medium/53/44/afdfea246e5c2aec0d1d888da5da.jpg"
					alt="#ilovefs"
				/>
			</figure>
	
			<p>Wat is uw bijdrage aan deze speciale dag, toegewijd aan de mensen die u in staat stellen om Vrije Software te gebruiken? Zult u onze stickers en ballonnen gebruiken? Of een afbeelding of video met uw nieuwe ILoveFS-shirt maken? En hoe zit het met het vieren van softwarevrijheid met uw collega's en vrienden op een bedrijfsbijeenkomst of publiek evenement? Wat u ook doet, laat het aan de wereld zien door de <em>#ilovefs</em>-tag te gebruiken. En <a href="/contact/">stuur ons gewoon een e-mail</a> als u vragen heeft.</p>
	
			<p>Vrolijke <strong><span class="text-danger">Ik hou van Vrije Software dag</span></strong> iedereen! ❤</p>
		</module>
	</data>
